| Command | Mean [ms] | Min [ms] | Max [ms] | Relative |
|:---|---:|---:|---:|---:|
| `sed -i 's/[ \t]*$//' /tmp/moby-dick-ws.txt` | 64.3 ± 23.0 | 53.6 | 217.8 | 45.10 ± 27.05 |
| `busybox sed -i 's/[ \t]*$//' /tmp/moby-dick-ws.txt` | 64.2 ± 23.4 | 53.9 | 178.7 | 45.05 ± 27.19 |
| `toybox sed -i 's/[ \t]*$//' /tmp/moby-dick-ws.txt` | 83.1 ± 4.6 | 77.9 | 108.2 | 58.31 ± 28.27 |
| `gawk -i inplace '{sub(/[ \t]+$/,"");print}' /tmp/moby-dick-ws.txt` | 33.4 ± 15.2 | 23.7 | 108.2 | 23.45 ± 15.54 |
| `perl -pil -e 's/[ \t]+$//' /tmp/moby-dick-ws.txt` | 39.4 ± 25.1 | 29.8 | 258.9 | 27.66 ± 22.11 |
| `sd '[ \t]+$' /tmp/moby-dick-ws.txt` | 1.4 ± 0.7 | 0.8 | 7.9 | 1.00 |
