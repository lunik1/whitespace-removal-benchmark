#! /usr/bin/env python3

import numpy as np
import random

rng = np.random.default_rng(seed=42)
random.seed(42)

text = ""

with open("moby-dick.txt", "r") as f:
    while (line := f.readline()):
        line = line.rstrip()

        n_space = rng.poisson(2, 1)[0]
        n_tab = rng.poisson(1, 1)[0]

        ws = [" "] * n_space + ["\t"] * n_tab
        random.shuffle(ws)

        text += line + "".join(ws) + "\n"

with open("moby-dick-ws.txt", "w") as f:
    f.write(text)
