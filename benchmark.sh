#!/usr/bin/env sh

./whitespacer.py

hyperfine \
    --prepare 'cp moby-dick-ws.txt /tmp' \
    --cleanup 'rm /tmp/moby-dick-ws.txt' \
    --warmup 5 \
    --min-runs 100 \
    --export-markdown "README.md" \
    'sed -i '\''s/[ \t]*$//'\'' /tmp/moby-dick-ws.txt' \
    'busybox sed -i '\''s/[ \t]*$//'\'' /tmp/moby-dick-ws.txt' \
    'toybox sed -i '\''s/[ \t]*$//'\'' /tmp/moby-dick-ws.txt' \
    'gawk -i inplace '\''{sub(/[ \t]+$/,"");print}'\'' /tmp/moby-dick-ws.txt' \
    'perl -pil -e '\''s/[ \t]+$//'\'' /tmp/moby-dick-ws.txt' \
    'sd '\''[ \t]+$'\'' /tmp/moby-dick-ws.txt'

rm moby-dick-ws.txt
